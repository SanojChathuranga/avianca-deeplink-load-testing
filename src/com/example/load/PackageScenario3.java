package com.example.load;

 
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;

//import com.gargoylesoftware.htmlunit.WebConsole.Logger;

 
public class PackageScenario3 extends AbstractJavaSamplerClient implements Serializable {
    private static final long serialVersionUID = 1L;
 
    // set up default arguments for the JMeter GUI
    @Override
    public Arguments getDefaultParameters() {
        Arguments defaultParameters = new Arguments();
        defaultParameters.addArgument("URL", "http://stgn-aviancatours-mcs4.secure-reservation.com");
        defaultParameters.addArgument("Proxy", "false");
        //defaultParameters.addArgument("SEARCHFOR", "Def search");
        return defaultParameters;
     }
 
    @SuppressWarnings("deprecation")
	@Override
    public SampleResult runTest(JavaSamplerContext context) {
    	
        // pull parameters
    	//String Url              = "http://secure.travelstay.com/servlet/TravelStay.ResevationSearch?afCd=RA&afTp=null&NoAd=2&NoCh=0&NoNgt=1&stPr=0&edPr=999999&PrRg=All%20Prices&AcNm=All%20Accommodations&Grp=%25&NoRm=0&Ct=CY1&InDt=INDATE&InMn=INMONTH&InYr=INYEAR&OtDt=OUTDATE&prSc=http%3A//bookings.travelstay.com/SearchTypes_new.htm&OtMn=OUTMONTH&OtYr=OUTYEAR&htCd=%25&amty=%25&slCt=London&CtgNm=All&slCat=%25&Dcn1=N&Dcn2=N&mxP=99999&slLoc=%25&sLNm=All&flB=C&Sor=W&vInf=-&language=en&country=US&InDate=undefined&OutDate=undefined&NoAdTs=ADNUM&cny=GBP&roomtype=&browser=IE&Utp=HT&id=TS&sgamty=-&bcd=&RmTyD=&RmTyp=GRT2%2CGRT3%2CGRT6&newquery=Y";
    	String urlString = context.getParameter( "URL" );
    	String Url				= urlString+"/aviancaReservations/search.do?method=search&DLR38=V&dlr=Y&DLR34=Bogota|BOG|259|Bogota%20Eldorado%20Airport|-|187||Colombia&DLR36=Aruba|AUA|54| Reina%20Beatrix%20International%20Airport|-|152||USA&DLR23=INDATE/INMONTH/INYEAR&DLR24=OUTDATE/OUTMONTH/OUTYEAR& DLR39=00:30:00&DLR40=00:30:00&DLR21=Economy&DLR8=en_US&DLR12=1|0|-|%25|%25@&DLR20=0&DLR22=R&DLR44=Y&DLR45=N";
    	
    	String[] dates = new String[2];
    	Random    rand = new Random(); 
    	int          n = rand.nextInt(50) + 7;
    	Calendar   cal = Calendar.getInstance();

    	cal.add(Calendar.DATE, n);
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	dates[0 ]      = sdf.format(cal.getTime());
    	cal.add(Calendar.DATE, 3);
    	dates[1]       = sdf.format(cal.getTime());
    	
    	String[] InStr      = dates[0].split("/");
    	String[] OutStr     = dates[1].split("/");

    	String Deeplink = Url.replace("INDATE",InStr[0]).replace("INMONTH",InStr[1]).replace("INYEAR",InStr[2]).replace("OUTDATE",OutStr[0]).replace("OUTMONTH",OutStr[1]).replace("OUTYEAR",OutStr[2]).replace("ADNUM","3");
        String Proxy    = context.getParameter( "Proxy" );
        System.out.println(Deeplink);
 /*       org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("LogName");
        logger.info(Deeplink);*/
        
        System.out.println(Deeplink);
        
        if(Proxy.trim().equalsIgnoreCase("true"))
        {
        	System.setProperty("proxySet", "true");
            System.setProperty("http.proxyHost", "192.168.1.132");
            System.setProperty("http.proxyPort", "3128");
            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {

                    return new PasswordAuthentication("dulan","123456".toCharArray());
                }
            });
        }
      
     
        SampleResult result = new SampleResult();
        result.sampleStart(); // start stopwatch
         
        try {
            java.net.URL url = new java.net.URL(Deeplink);
            java.net.HttpURLConnection connection = (
                java.net.HttpURLConnection
            )url.openConnection(); // have to cast connection
            connection.setRequestMethod("GET");
            
            connection.getResponseCode();
            
       		BufferedReader in = new BufferedReader( new InputStreamReader(connection.getInputStream()));
       		String inputLine;
       		StringBuffer response = new StringBuffer();

       		while ((inputLine = in.readLine()) != null) {
       			response.append(inputLine);
       		}
       		in.close();

       		//print result
       		System.out.println(response.toString());
 
            result.sampleEnd(); // stop stopwatch
            result.setSuccessful( true );
            result.setResponseMessage(response.toString());
            result.setResponseCodeOK(); // 200 code
        } catch (Exception e) {
            result.sampleEnd(); // stop stopwatch
            result.setSuccessful( false );
            result.setResponseMessage( "Exception: " + e );
 
            // get stack trace as a String to return as document data
            java.io.StringWriter stringWriter = new java.io.StringWriter();
            e.printStackTrace( new java.io.PrintWriter( stringWriter ) );
            result.setResponseData( stringWriter.toString() );
            result.setDataType( org.apache.jmeter.samplers.SampleResult.TEXT );
            result.setResponseCode( "500" );
        }
 
        return result;
    }
}